# awebot plugins [![Build Status](https://travis-ci.org/aatxe/awebot-plugins.svg?branch=master)](https://travis-ci.org/aatxe/awebot-plugins) #
A collection of plugins for [awebot](https://github.com/aatxe/awebot).
